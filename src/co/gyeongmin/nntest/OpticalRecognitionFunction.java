package co.gyeongmin.nntest;

import co.gyeongmin.neuralnet.NeuralNetLearning;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OpticalRecognitionFunction {
    private final int ORIGINAL_IMG_SIZE_WIDTH = 32;
    private final int ORIGINAL_IMG_SIZE_HEIGHT = 32;
    private int SIZE_WIDTH = 32;
    private int SIZE_HEIGHT = 32;
    private int SIZE_AREA = SIZE_WIDTH * SIZE_HEIGHT;
    private int MOVE_UNIT_WIDTH = 32 / SIZE_WIDTH;
    private int MOVE_UNIT_HEIGHT = 32 / SIZE_HEIGHT;

    private final double VALUE_MAX = 9.0;
    private final double VALUE_MIN = 0.0;

    private String filename;
    private NeuralNetLearning neuralNetLearning;
    private List<TestSet> testSetList;
    public OpticalRecognitionFunction(String filename, int compressionFactor) throws IOException {
        this.filename = filename;
        this.neuralNetLearning = new NeuralNetLearning(0.45, 0.9, new int[]{512, 256, 128, 64, 32, 16, 8, 4, 2, 1}, SIZE_WIDTH * SIZE_HEIGHT, 1);
        this.testSetList = new ArrayList<>();

        if (compressionFactor > 0) {
            SIZE_WIDTH /= compressionFactor;
            SIZE_HEIGHT /= compressionFactor;
            SIZE_AREA = SIZE_WIDTH * SIZE_HEIGHT;
            MOVE_UNIT_WIDTH = ORIGINAL_IMG_SIZE_WIDTH / SIZE_WIDTH;
            MOVE_UNIT_HEIGHT = ORIGINAL_IMG_SIZE_HEIGHT / SIZE_HEIGHT;
        }
        study();
    }

    private void readTestSetList() throws IOException {
        int iterate = 10;
        BufferedReader br = new BufferedReader(new FileReader(filename));

        for (String temp = br.readLine(); iterate-- > 0 && temp != null; temp = br.readLine()) {
            String[] lines = new String[32];

            lines[0] = temp;
            for (int i = 1; i < 32; i++) {
                lines[i] = br.readLine();
            }

            temp = br.readLine();
            temp = temp.replaceAll(" ", "");
            int value = Integer.valueOf(temp);

            TestSet ts = new TestSet(lines, value);
            testSetList.add(ts);
        }
    }

    private void study() throws IOException {
        readTestSetList();

        double range = VALUE_MAX - VALUE_MIN;

        for (int i = 0; i < 10000; i++) {
            testSetList.forEach(ts -> {
                neuralNetLearning.forward(ts.data);
                neuralNetLearning.backPropagate(new double[]{(ts.answer - VALUE_MIN) / range});
            });

            if ((i + 1) % 2000 == 0) {
                System.out.println("#" + (i + 1) + " times studied.");
            }
        }
    }

    private double areaAverage(byte[][] data, int startX, int startY) {
        double sum = 0.0;
        for (int i = startX; i < startX + MOVE_UNIT_HEIGHT; i++) {
            for (int j = startY; j < startY + MOVE_UNIT_WIDTH; j++) {
                sum += data[i][j] - '0';
            }
        }

        return Math.round(sum / SIZE_AREA);
    }

    public int number(byte[][] data) {

        double[] dblData = new double[SIZE_WIDTH * SIZE_HEIGHT];

        for (int i = 0; i < data.length; i += MOVE_UNIT_HEIGHT) {
            for (int j = 0; j < data[i].length; j += MOVE_UNIT_WIDTH) {
                int iIdx = i / MOVE_UNIT_HEIGHT;
                int jIdx = j / MOVE_UNIT_WIDTH;
                double value = areaAverage(data, i, j);
                dblData[iIdx * MOVE_UNIT_WIDTH + jIdx] = value;
            }
        }

        neuralNetLearning.forward(dblData);
        return (int)Math.round(neuralNetLearning.getOutput()[0] * (VALUE_MAX - VALUE_MIN) + VALUE_MIN);
    }

    private class TestSet {
        private double[] data;
        private int answer;

        TestSet(String[] lines, int value) {

            data = new double[SIZE_AREA];
            saveData(lines);
            this.answer = value;
        }

        private void saveData(String[] lines) {
            byte[][] tempData = new byte[lines.length][];

            for (int i = 0; i < tempData.length; i++) {
                tempData[i] = lines[i].getBytes();
            }

            for (int i = 0; i < lines.length; i += MOVE_UNIT_HEIGHT) {
                for (int j = 0; j < 32; j += MOVE_UNIT_WIDTH) {
                    int iidx = i / MOVE_UNIT_HEIGHT,
                        jidx = j / MOVE_UNIT_WIDTH;
                    data[iidx * MOVE_UNIT_WIDTH + jidx] = areaAverage(tempData, i, j);
                }
            }
        }
    }
}
